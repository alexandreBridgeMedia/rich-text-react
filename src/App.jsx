import React from "react";
// import MarkdownEditorTinyMCE from "./components/MarkdownEditorTinyMCE";
import MarkdownEditorMantine from "./components/MarkdownEditorMantine";

function App() {
  return (
    <div className="App">
      {/*<MarkdownEditorTinyMCE/>*/}
      {/*<hr/>*/}
      <MarkdownEditorMantine/>
    </div>
  )
}

export default App
