import React, {useState} from 'react';

import {RichTextEditor} from '@mantine/rte';

//https://www.npmjs.com/package/showdown
//https://github.com/showdownjs/showdown/wiki/Markdown's-XSS-Vulnerability-(and-how-to-mitigate-it)
import showdown from "showdown";
//https://www.npmjs.com/package/dompurify
import DOMPurify from 'dompurify';
import dangerousXssStringExample from "../dangerousXssStringExample";


const converter = new showdown.Converter({
  ghCodeBlocks: false,
  openLinksInNewWindow: true,
  tables: false,
  // simpleLineBreaks: true,
})
console.log("converter options:")
console.log(converter.getOptions());


const controls = [
  // ['underline', 'strike'],
  ['bold', 'italic'],
  // ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
  ['unorderedList', 'orderedList'],
  ['link'],
  ['clean'],
]

const labels = {
  save: 'Enregistrer',
  edit: 'Modifier',
  remove: 'Retirer',
  bold: 'Gras',
  italic: 'Italic',
  underline: 'Souligné',
  strike: 'Barré',
  link: 'Lien Internet',
  unorderedList: 'Liste pointée',
  orderedList: 'Liste numérotée',
  clean: 'Réinitialiser les styles',
  video: 'Embed video',
  alignCenter: 'Align text to center',
  alignLeft: 'Align text to left',
  alignRight: 'Align text to right',
  image: 'Embed image',
  h1: 'Titre 1',
  h2: 'Titre 2',
  h3: 'Titre 3',
  h4: 'Titre 4',
  h5: 'Titre 5',
  h6: 'Titre 6',
  sup: 'Super script',
  sub: 'Sub script',
  code: 'Code',
  codeBlock: 'Code block',
  blockquote: 'Block quote',
}

const styles = {
  width: "70ch"
}


const MarkdownEditorMantine = ({initialMarkdownValue = dangerousXssStringExample}) => {
  const [markdownValue, setMarkdownValue] = useState(() => DOMPurify.sanitize(initialMarkdownValue));
  const [htmlEditorValue, setHtmlEditorValue] = useState(() => converter.makeHtml(markdownValue));

  const handleEditorChange = (newHtmlValue) => {
    setHtmlEditorValue(newHtmlValue)

    const cleanedHtml = DOMPurify.sanitize(newHtmlValue)
    const markdownString = converter.makeMarkdown(cleanedHtml)
    setMarkdownValue(markdownString)
  }

  return (
    <div
      data-color-mode="light"
      style={{display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center", width: "100%"}}
    >
      <h1>React Rich Text Editor Component by Mantine.dev</h1>
      <a href="https://mantine.dev/others/rte/" target="_blank" rel="noreferrer">
        https://mantine.dev/others/rte/
      </a>
      <br/>
      <br/>
      <RichTextEditor
        value={htmlEditorValue}
        onChange={handleEditorChange}
        controls={controls}
        labels={labels}
        style={styles}
      />


      <div style={{width: "100%"}}>
        <br/>
        <br/>
        <hr/>
      </div>


      <h2>Markdown output</h2>
      <a href="https://github.com/showdownjs/showdown" target="_blank" rel="noreferrer">
        npm <strong>showdown</strong> package: a two-way html/markdown converter
      </a>
      <br/>
      <br/>
      <textarea
        rows={15}
        cols={70}
        value={markdownValue}
        readOnly
      />


      <h2>HTML output</h2>
      <textarea
        rows={15}
        cols={70}
        value={htmlEditorValue}
        readOnly
      />

    </div>
  );
};


export default MarkdownEditorMantine;
