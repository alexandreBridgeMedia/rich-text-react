import React, {useRef, useState} from 'react';
import {Editor} from '@tinymce/tinymce-react';
import showdown from "showdown"; //https://www.npmjs.com/package/showdown
import DOMPurify from 'dompurify';
import dangerousXssStringExample from "../dangerousXssStringExample"; //https://www.npmjs.com/package/dompurify


const converter = new showdown.Converter()


const options = {
  height: 500,
  menubar: true,
  plugins: [
    'advlist autolink lists link charmap preview anchor',
    'searchreplace visualblocks fullscreen',
    'insertdatetime table paste help wordcount'
  ],
  toolbar: 'undo redo | formatselect | ' +
    'bold italic backcolor | alignleft aligncenter ' +
    'alignright alignjustify | bullist numlist outdent indent | ' +
    'removeformat | help',
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
}


const MarkdownEditorTinyMCE = ({initialValue = dangerousXssStringExample}) => {
  const editorRef = useRef(null);
  const [markdownValue, setMarkdownValue] = useState(initialValue);
  const [htmlValue, setHtmlValue] = useState(converter.makeHtml(initialValue));

  const handleEditorChange = (newValue, editor) => {
    const htmlContent = DOMPurify.sanitize(newValue)
    setHtmlValue(htmlContent)

    const markdownString = converter.makeMarkdown(htmlContent)
    setMarkdownValue(markdownString)
  }

  return (
    <div
      data-color-mode="light"
      style={{display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center", width: "100%"}}
    >

      <h1>MarkdownEditorTinyMCE</h1>
      <Editor
        apiKey="umf4e6o0xekltnrkogtg68dfus8upnfm4znrbzqz2a46ldfe"
        onInit={(evt, editor) => editorRef.current = editor}
        init={options}
        value={htmlValue}
        onEditorChange={handleEditorChange}
      />

      <textarea readOnly cols="30" rows="10" value={markdownValue}/>

    </div>
  );
};


export default MarkdownEditorTinyMCE;
