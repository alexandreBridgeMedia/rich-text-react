const dangerousXssStringExample = `
**Hello world!!!** 

<IFRAME SRC="javascript:javascript:alert(window.origin);"></IFRAME>
<script>alert(1)</script>
<iframe src="https://example.com"></iframe>
This is a malicious [XSS](javascript:alert(1)) attempt that is cleaned by DOMPurify.

<script>
require('child_process').spawn('echo', ['hack!']);
</script>

Hello <a name="n"> href="javascript:alert('xss')"> *you* </a>

<br>

**Hey**

<br>

I'm **bold**, and ***affirmative*** !

What about *that simple italic* ?

Is this better ?
Yes. Surely with [this nice external link](https://finance-d.com)
`

export default dangerousXssStringExample
